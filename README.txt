###########################################################################################
# Package name: Persian Speech Corpus                                                     #
# Author: Nawar HALABI (nawar.halabi@gmail.com)                                           #
# Owner: MicroLinkPC (UK) LIMITED (wpateam@microlinkpc.com)                               #
# Version: 2.0                                                                            #
# License: Commercial (Commercial use only allowed after agreement with OWNER).           #
#                                                                                         #
# Thank you for downloading the corpus. The corpus is free for research purposes. For a   #
# technical support or commercial license details, please contact OWNER or AUTHOR.        #
#                                                                                         #
# Please ignore the commerical license requirement if you have already aquired one        #
# through one of our distributors.                                                        #
###########################################################################################

1- The 'lab' subdirectories contain label files with the phonetic transcirpt in
   them corresponding to each text grid.

2- The file 'orthographic-transcript.txt' is simply all the orthgraphic transcripts merged
   and paired with their corresponding wav file name.

3- The 'textgrid' subdirectories contain praat files with the phonetic transcirpt  and
   timetamps (alignments). Please read more online about the textgrid format. Note that
   the file 'phonetic-transcript.txt' is simply all the phonetic transcripts merged
   without timestamps paired with their corresponding wav file name.

4- In orthographic transcripts a pause is resembled with a punctuation. In phonetic
   transcripts it is resembled with 'sil' or 'wb' (word boundary).

5- For more details on the phonemes used in the phonetic transcirpt, please refer to the
   phones.txt file.
   
6- aligned.mlf contains the alignment of all the files in a HTS friendly format.